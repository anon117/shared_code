"""
Necessary: Python 3.x
Make a directory and copy this file into it. 
This code send an email to the recepients with the attached files.
The files what you want to send copy into a directory named 'attachments', then create a file named mail_addresses.txt.
One line, one mail address!

The code will read the mail address, and scan your all files from the attachment directory, and attach them to the mail.

2013.07.06
by anon117
"""

import smtplib, os, os.path, tempfile

import email.encoders
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.header import Header

class sendmail_multipart(object):
	"""docstring for sendmail_multipart"""
	def __init__(self, arg):
		super(sendmail_multipart, self).__init__()
		self.arg = arg
		self.my_gmail_login = 'xxxx@gmail.com'
		self.my_gmail_passwd = 'xxxx'
		self.mailfrom = 'xxxx@gmail.com'
		self.smtp_host = 'smtp.gmail.com'
		self.smtp_port = 587
		self.attached = "attachments" # ez egy konyvtar
		self.list_of_attach = []
		self.targy = ''
		self.text = ''
		self.mailto = ['yyy@gmail.com']
		self.mail_cimek = 'mail_cimek.txt' # az utolso email cim utan ne rakj <Enter>-t
		self.mail_cimek_list = []
		self.tempfilename = ''

	def setSubject(self):
		self.targy = input('Targy: ')

	def setText(self):
		# self.text = input('Szoveg: ')
		self.tempfilename = tempfile.NamedTemporaryFile(suffix=".tmp")
		os.system('editor ' + self.tempfilename.name)
		self.tempfilename.flush()
		self.text = self.tempfilename.read()

	def files2list(self):
		for elem in os.listdir(self.attached):
			if os.path.isfile(os.path.join(self.attached, elem)):
				print (elem)
				self.list_of_attach.append(os.path.basename(elem))
		print (self.list_of_attach)

	def mailBuild(self):
		message = MIMEMultipart()
		message.set_charset('utf-8')
		message['Subject'] = Header(self.targy, 'utf-8')
		message['To'] = 'kodexhelp@gmail.com'
		message['From'] = 'bodnar.zs.a@gmail.com'
		if os.path.basename(self.mail_cimek):
			if os.stat(self.mail_cimek).st_size > 0:
				self.mail_cimek_list = [line.strip() for line in open(self.mail_cimek, 'r')]
				# fd_mailcimek.close()
				delimiter = ", "
				message['Bcc'] = delimiter.join(self.mail_cimek_list)		
		
		message.attach(MIMEText(self.text, _charset='utf-8'))
		
		for csat_file in self.list_of_attach:
			print (os.path.join(self.attached, csat_file))
			csatolmany = MIMEBase('application', 'octet-stream')
			csatolmany.set_payload(open(os.path.join(self.attached, csat_file), 'rb').read()) # Ez lesz majd csatolva... beolvassa
			email.encoders.encode_base64(csatolmany) # base64 kodolas 7bitre
			csatolmany.add_header('Content-Disposition', 'attachment; filename="%s"' % os.path.basename(csat_file))
			# a fenti fejlec irja, h mi is a neve a csatolmanynak
			message.attach(csatolmany) # A kesz csatolmany hozzaadasa
		print (message.as_string())	
		self.sendmail(self.mailfrom, self.mailto, message)

	def sendmail(self, m_from, m_to, m_uzenet):
		try:
			server = smtplib.SMTP("smtp.gmail.com", 587)
			server.ehlo() # koszones
			server.starttls() # TLS
			server.login(self.my_gmail_login, self.my_gmail_passwd) # Auth...
			server.sendmail(m_from, [m_to], m_uzenet.as_string())
			server.close()
			self.tempfilename.close()
			print (':) - Sikeres kuldes...\n')
		except:
			print (':( - Hiba, nem lett elkuldve')

x = sendmail_multipart('')
x.setSubject()
x.setText()
x.files2list()
x.mailBuild()
